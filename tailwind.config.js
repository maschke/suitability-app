const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: ["./src/**/*.vue"],
  theme: {
    extend: {
      colors: {
        primary: defaultTheme.colors.gray[800],
        secondary: "#EE2E5D"
      }
    }
  },
  variants: {},
  plugins: []
};
