import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home.vue";

Vue.use(VueRouter);

// const AsyncViewComponent = () => ({
//   component: import("../views/About.vue"),
//   loading: LoadingComponent,
//   error: ErrorComponent,
//   delay: 200,
//   timeout: 3000
// });

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/suitability",
    name: "Suitability",
    component: () => import("@/views/Suitability.vue")
  }
];

const router = new VueRouter({
  routes
});

export default router;
