import Vue from "vue";
import Notifications from "vue-notification";
import App from "./App.vue";
import router from "./router";

import "./main.css";

Vue.config.productionTip = false;

Vue.use(Notifications);

new Vue({
  router,
  // eslint-disable-next-line no-unused-vars
  render: h => <App />
}).$mount("#app");
