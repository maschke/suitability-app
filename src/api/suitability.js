const CONTEXT = "suitability";

export const sendMessage = (message = {}) =>
  fetch(`${process.env.VUE_APP_API_URL}/conversation/message`, {
    method: "POST",
    body: JSON.stringify({
      ...message,
      context: CONTEXT
    }),
    headers: {
      "content-type": "application/json"
    }
  }).then(res => res.json());

export const finish = (data = {}) =>
  fetch(`${process.env.VUE_APP_API_URL}/suitability/finish`, {
    method: "POST",
    body: JSON.stringify({
      ...data,
      context: CONTEXT
    }),
    headers: {
      "content-type": "application/json"
    }
  }).then(res => res.json());
