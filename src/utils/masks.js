const DEFAULT_MAX_LENGTH = 100;
const EMAIL_MAX_LENGTH = 50;
const CURRENCY_MAX_LENGTH = 17;

const createMask = (maxLength, maskFn) => str => {
  const value = str.length <= maxLength ? str : str.slice(0, maxLength);
  if (maskFn) {
    return maskFn(value);
  }
  return value;
};

const currency = (str = "") => {
  const isNumber = /^\d*$/.test(str);
  const value = isNumber ? str : 0;

  return new Intl.NumberFormat("pt-BR", {
    style: "currency",
    currency: "BRL",
    minimumFractionDigits: 2
  }).format(value / 100);
};

const email = (str = "") => str.replace(/[^\S+@\S+.\S+]$/g, "");

const integer = (str = "") => {
  return new Intl.NumberFormat("pt-BR").format(
    Number(str.replace(/[^\d]/g, ""))
  );
};

const name = (str = "") => {
  return str.replace(/[^A-Za-z ]/g, "");
};

const masks = {
  currency: createMask(CURRENCY_MAX_LENGTH, currency),
  email: createMask(EMAIL_MAX_LENGTH, email),
  name: createMask(DEFAULT_MAX_LENGTH, name),
  integer
};

export function mask(type, str) {
  if (!masks[type]) {
    throw new Error(`Mask Utils: Invalid mask type ${type}`);
  }

  return masks[type](str);
}

export function unmask(str, preserveChars = []) {
  return String(str)
    .split("")
    .reduce((acc, char) => {
      if (!/^\d+$/.test(char) && !preserveChars.includes(char)) return acc;

      return acc + char;
    }, "");
}
