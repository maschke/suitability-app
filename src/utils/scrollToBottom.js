export default () =>
  window.scrollTo({
    top: document.body.scrollHeight,
    behavior: "smooth"
  });
