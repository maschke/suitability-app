import { action } from "@storybook/addon-actions";
import Message from "../Message.vue";

export default {
  title: "Message",
  component: Message
};

export const Default = () => ({
  components: { Message },
  template: `
    <Message
      text="Qual é a sua renda mensal? ^300 Esta informação vai me ajudar a montar sugestões feitas exclusivamente para você (e é sigilosa também)."
      @loaded="handleLoaded"
    />
  `,
  methods: {
    handleLoaded: action("loaded message")
  }
});

export const ReplyMessage = () => ({
  components: { Message },
  template: `
    <Message
      text="R$ 2.000,00"
      :reply="true"
    />
  `
});

export const LoadingMessage = () => ({
  components: { Message },
  template: `
    <Message
      text=""
      :loading="true"
    />
  `
});

export const EraseMessage = () => ({
  components: { Message },
  template: `
    <Message
      text="Você prefere Erasmo ou Roberto Carlos? ^1000 <erase>(ops... chat errado!) ^1000"
    />
  `
});
