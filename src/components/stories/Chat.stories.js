import { action } from "@storybook/addon-actions";
import Chat from "../Chat.vue";

export default {
  title: "Chat",
  component: Chat
};

export const Default = () => ({
  components: { Chat },
  template: `
    <Chat
      :messages="[
        {
          value: 'Qual é a sua renda mensal? ^300 Esta informação vai me ajudar a montar sugestões feitas exclusivamente para você (e é sigilosa também).'
        },
        {
          value: 'R$ 2.000,00',
          reply: true
        }
      ]"
      @loadedAll="handleLoaded"
    />
  `,
  methods: {
    handleLoaded: action("loaded message")
  }
});

// export const ReplyMessage = () => ({
//   components: { Message },
//   template: `
//     <Message
//       text="R$ 2.000,00"
//       :reply="true"
//     />
//   `
// });

// export const LoadingMessage = () => ({
//   components: { Message },
//   template: `
//     <Message
//       text=""
//       :loading="true"
//     />
//   `
// });
