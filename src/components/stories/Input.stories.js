import { action } from "@storybook/addon-actions";
import Input from "../Input.vue";

export default {
  title: "Input",
  component: Input,
  decorators: [
    () => ({
      template: `
      <div style="display: flex; background: #EBECF3; padding: 30px;">
        <story/>
      </div>
    `
    })
  ]
};

export const Text = () => ({
  components: { Input },
  template: `
    <Input name="name" @input="onChange" :autofocus="true"></Input>
  `,
  methods: {
    onChange: action("change")
  }
});

export const Currency = () => ({
  components: { Input },
  template: `
    <Input value="0" name="salary" mask="currency" @input="onChange" :autofocus="true"></Input>
  `,
  methods: {
    onChange: action("change")
  }
});

export const Email = () => ({
  components: { Input },
  template: `
    <Input name="email" mask="email" @input="onChange" :autofocus="true"></Input>
  `,
  methods: {
    onChange: action("change")
  }
});

export const Number = () => ({
  components: { Input },
  template: `
    <Input name="age" mask="integer" @input="onChange" :autofocus="true" maxlength="3"></Input>
  `,
  methods: {
    onChange: action("change")
  }
});

export const Name = () => ({
  components: { Input },
  template: `
    <Input name="name" mask="name" @input="onChange" :autofocus="true"></Input>
  `,
  methods: {
    onChange: action("change")
  }
});

export const ControlledInput = () => ({
  components: { Input },
  template: `
    <div style="flex: 1">
      <Input name="name" v-model="value" :autofocus="true"></Input>
      <p>Value is: {{ value }}</p>
    </div>
  `,
  data: function() {
    return {
      value: ""
    };
  }
});
