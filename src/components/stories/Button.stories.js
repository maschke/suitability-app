import { action } from "@storybook/addon-actions";
import Button from "../Button.vue";

export default {
  title: "Button",
  component: Button
};

export const Primary = () => ({
  components: { Button },
  template: `<Button @click="onClick" variant="primary">Primary</Button>`,
  methods: {
    onClick: action("click")
  }
});

export const Secondary = () => ({
  components: { Button },
  template: `<Button @click="onClick" variant="secondary">Secondary</Button>`,
  methods: {
    onClick: action("click")
  }
});

export const Link = () => ({
  components: { Button },
  template: `<Button as="a" href="https://warren.com.br/" target="_blank">Link</Button>`
});
